using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinuteHand : ClockHand
{
    public MinuteHand(GameObject clockMinuteHand) : base(clockMinuteHand) {}    
    public void SetInitialGrades(int gradesPerMinute)
    {
        InitialGrades = System.DateTime.Now.Minute * gradesPerMinute;
        ActualGrades = InitialGrades;
    }
    new public void RotateHandToInitialPosition()
    {
        base.RotateHandToInitialPosition();
    }

    public void UpdateMinutes(int grades)
    {
        base.UpdateHand(grades);
    }
}
